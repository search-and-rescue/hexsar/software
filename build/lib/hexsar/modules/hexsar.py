#! /usr/bin/env python3

from hexsar.modules.leg import Leg

class HexSaR:
    def __init__(self):

        self.leg1 = Leg()
        self.leg1.join0 = leg1.register_servo(0, 0x40)
        self.leg1.join1 = leg1.register_servo(1, 0x40)
        self.leg1.join2 = leg1.register_servo(2, 0x40)

        self.leg2 = Leg()
        self.leg3 = Leg()
        self.leg4 = Leg()
        self.leg5 = Leg()
        self.leg6 = Leg()
