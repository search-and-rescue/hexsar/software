#! /usr/bin/env python3

from sarcore.modules.core import SARCore


class Leg:
    def __init__(self):
        self.join0 = None
        self.join1 = None
        self.join2 = None

    def register_servo(self, channel, address):
        core = SARCore()
        core.actuators.discover('servo', address)
        core.actuators.servo.set_channel(channel)

        return core.actuators.servo

    def set_angles(self, angles):
        if type(angles) is tuple:
            if len(angles) == 3:
                self.join0.angle = angles[0]
                self.join1.angle = angles[1]
                self.join2.angle = angles[2]


if __name__ == '__main__':
    leg = Leg()
    leg.join0 = leg.register_servo(0, 0x40)
    leg.join1 = leg.register_servo(1, 0x40)
    leg.join2 = leg.register_servo(2, 0x40)

    core = SARCore()
    core.actuators.discover('servo', 0x40)
    core.actuators.servo.enable()





