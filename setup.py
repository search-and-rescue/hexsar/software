import os
import glob
import subprocess

from setuptools import setup, find_packages
from pip._internal.req import parse_requirements


def get_version(cmd):
    output = subprocess.check_output(cmd, shell=True).strip()
    return output.decode('utf-8')


def get_install_reqs(file):
    reqs = parse_requirements(file, session=False)
    return [req.requirement for req in reqs]


def get_long_description(file):
    here = os.path.abspath(os.path.dirname(__file__))
    with open(os.path.join(here, file), encoding='utf-8') as fp:
        long_description = fp.read()

    return long_description


def data_files():
    dest = 'hexsar'

    df = [(dest, glob.glob(os.path.join('python', 'bin', 'hexsar.conf')))]

    return df

setup(
    name='hexsar',
    version=get_version('git describe --tags --abbrev=0'),
    description='HexSaR Modules',
    long_description=get_long_description('README.md'),
    author='CIII',
    license='GNU GPLv3',
    package_dir={'hexsar': 'python/hexsar',
                 'hexsar.modules': 'python/hexsar/modules',
                  },
    packages=['hexsar',
              'hexsar.modules'],
    include_package_data=True,
    data_files=data_files(),
    scripts=[os.path.join('python', 'bin', 'hexsar')],
    install_requires=get_install_reqs('requirements.txt'),
)
